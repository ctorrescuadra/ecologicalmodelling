# Exergoecoloy as a tool for ecological modelling.
### The case of the North American Food Chain


**Paper publised in:** Ecological Modelling
 
**Authors:** Antonio Valero, Alicia Valero y César Torres

## Abstract

Exergoecology and in particular, thermoeconomic analysis is used to understand the process of cost formation and to improve the design and the operation of extensive energy consumption systems such as power and chemical plants. This paper shows the capabilities for using the thermoeconomic analysis in environmental systems, and demonstrates that it could become a useful tool for identifying the ways for improving the energy resources cost and the efficiency of a macroeconomic system such as the US food production 
chain. The environmental impact associated  with each process in the food 
production chain can be quantified through a thermoeconomic approach as a cost function, which represents the required natural resources to obtain a final 
product. In the example provided, several simulations such as the impact of the change of meat diet basis for a vegetarian diet, and reusing the residual biomass are analyzed.
